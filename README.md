# eRSO status Dell

This is an optional role for [rso-mirror](https://gitlab.com/duh-casa/rso-mirror), useful on compatible Dell servers, which keeps the little LCD screen on the server updated with the current connection / replication status.

This was made since one of the servers recycled in the course of operation of the [RSO project](https://racunalniki.duh-casa.si/), was an old Dell server, which harbors this little 5-character LCD screen on the bezel. It's primary purpose is to display diagnostic messages in case of malfunction, but it is programable, so we made it display the status of the functions of our server software.

Obviously this does not have any useful value and is done purely for nerd points. However it is my hope that by making working code for this available to you, and since our code generally adheres to good programming practices, that it will allow you to quickly adapt it for your own use.

# Compatibility

Code was tested on CentOS 7.9, which is what we had. It should work on other RHEL compatible platforms, though we recommend something that's approximately the same age as your hardware, to avoid potential problems with lack of platform support from Dell.

Appologies for poor code quality, you may have to adjust it quite a bit to work for you. Obviously this is just a quick hack, so there is no point for us to invest a lot of time into code we are never going to use.

# Dependencies

Needs: https://gitlab.com/duh-casa/rso-mirror and https://gitlab.com/duh-casa/rso-mirror-management

Basically the first installs PHP and similar and also deploys the second, which then contains the [code](https://gitlab.com/duh-casa/rso-mirror-management/-/blob/master/status.php) that actually checks the status of services. This PHP file returns JSON code otherwise used in an AJAX request, which can instead be parsed by `jq`.

None of this is required if you are only interested in updating the LCD with your own info.
